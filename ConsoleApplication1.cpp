﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter your offer: ";
    std::string offer;
    std::getline(std::cin, offer);

    std::cout << offer << "\n";
    std::cout << offer.length() << "\n";
    
    std::cout << offer[0] << std::endl; //вывод символа указан в квадратных скобках
    
    std::cout << offer[offer.size() - 2] << std::endl;
    
    
    return 0;
}
     
